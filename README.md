# SISTEMA TESTE: GERENCIAMENTO DE FUNCIONÁRIOS

Sistema de criação, edição e consulta de Funcionários, Cargos e Lotações, com permissões distintas de acesso a essas entidades, conforme as autorizações do usuário logado.

Projeto implementado com:
PHP, Doctrine, Docker, MySQL, Bootstrap e jQuery.

### Estrutura do DB:

![image](/uploads/592cb9d763b2506f0cb26d065a9528e5/image.png)

### Instruções:

1- Apagar o diretório `mysql/` da raiz do projeto, se existir.

2- Criar imagens:

```
$ docker-compose build
```

3- Subir os containers:

```
$ docker-compose up -d
```

4- Inserir mock data:

```
$ docker exec -it php74-container bash
container:/var/www# php -a
php> require('commands/seeder.php');
```

5- Acessar a página no browser:

`http://localhost:9000/`

6- Usuários disponíveis:

* login: mme10; senha: 123123; autorizações: cargos, lotações, usuários.

* login: ggi10; senha: 123123; autorizações: cargos, lotações.

* login: mfl10; senha: 123123; autorizações: cargos.

* login: bga10; senha: 123123; autorizações: -.

7- Parar os containers:

```
$ docker-compose down
```


