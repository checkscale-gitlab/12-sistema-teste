<?php

use App\Infra\EntityManagerCreator;

require_once __DIR__ . '/../vendor/autoload.php';

$entityManagerCreator = new EntityManagerCreator();
$entityManager = $entityManagerCreator->getEntityManager();

////////// Adicionar Cidades /////////////////////

$sqlInsertCidade = "INSERT INTO Cidade (sigla, nome) VALUES (:sigla, :nome);";
$statement = $entityManager->getConnection()->prepare($sqlInsertCidade);

$statement->bindValue(':sigla', 'CTB');
$statement->bindValue(':nome', 'Curitiba');
$statement->executeStatement();

$statement->bindValue(':sigla', 'APU');
$statement->bindValue(':nome', 'Apucarana');
$statement->executeStatement();

$statement->bindValue(':sigla', 'CMO');
$statement->bindValue(':nome', 'Campo Mourão');
$statement->executeStatement();

$statement->bindValue(':sigla', 'CAS');
$statement->bindValue(':nome', 'Cascavel');
$statement->executeStatement();

$statement->bindValue(':sigla', 'FOZ');
$statement->bindValue(':nome', 'Foz do Iguaçu');

if ($statement->executeStatement())
{
    echo "Cidade incluída com sucesso.\n";
}

////////// Adicionar Lotações /////////////////////

$sqlInsertLotacao = "INSERT INTO Lotacao (sigla, nome, cidade_id) VALUES (:sigla, :nome, :cidade_id);";
$statement = $entityManager->getConnection()->prepare($sqlInsertLotacao);

$statement->bindValue(':sigla', 'PRAPU01');
$statement->bindValue(':nome', '1ª Vara Federal de Apucarana');
$statement->bindValue(':cidade_id', 2);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRAPUDIR');
$statement->bindValue(':nome', 'Direção do Foro de Apucarana');
$statement->bindValue(':cidade_id', 2);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCMO01');
$statement->bindValue(':nome', '1ª Vara Federal de Campo Mourão');
$statement->bindValue(':cidade_id', 3);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCMO02');
$statement->bindValue(':nome', '2ª Vara Federal de Campo Mourão');
$statement->bindValue(':cidade_id', 3);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCMODIR');
$statement->bindValue(':nome', 'Direção do Foro de Campo Mourão');
$statement->bindValue(':cidade_id', 3);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCAS01');
$statement->bindValue(':nome', '1ª Vara Federal de Cascavel');
$statement->bindValue(':cidade_id', 4);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCAS02');
$statement->bindValue(':nome', '2ª Vara Federal de Cascavel');
$statement->bindValue(':cidade_id', 4);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRCASDIR');
$statement->bindValue(':nome', 'Direção do Foro de Cascavel');
$statement->bindValue(':cidade_id', 4);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRFOZ01');
$statement->bindValue(':nome', '1ª Vara Federal de Foz do Iguaçu');
$statement->bindValue(':cidade_id', 5);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRFOZ02');
$statement->bindValue(':nome', '2ª Vara Federal de Foz do Iguaçu');
$statement->bindValue(':cidade_id', 5);
$statement->executeStatement();

$statement->bindValue(':sigla', 'PRFOZDIR');
$statement->bindValue(':nome', 'Direção do Foro de Foz do Iguaçu');
$statement->bindValue(':cidade_id', 5);

if ($statement->executeStatement())
{
    echo "Lotação incluída com sucesso.\n";
}

////////// Adicionar Cargos /////////////////////

$sqlInsertCargo = "INSERT INTO Cargo (descricao, carga_horaria, dias_semana) VALUES (:descricao, :carga_horaria, :dias_semana);";
$statement = $entityManager->getConnection()->prepare($sqlInsertCargo);

$statement->bindValue(':descricao', 'Estágio');
$statement->bindValue(':carga_horaria', 20);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();

$statement->bindValue(':descricao', 'Analista Judiciário');
$statement->bindValue(':carga_horaria', 40);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();

$statement->bindValue(':descricao', 'Técnico Judiciário');
$statement->bindValue(':carga_horaria', 40);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();

$statement->bindValue(':descricao', 'Oficial de Justiça');
$statement->bindValue(':carga_horaria', 40);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();


$statement->bindValue(':descricao', 'Auxiliar Judiciário');
$statement->bindValue(':carga_horaria', 40);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();

$statement->bindValue(':descricao', 'Juiz Federal');
$statement->bindValue(':carga_horaria', 40);
$statement->bindValue(':dias_semana', 5);
$statement->executeStatement();

$statement->bindValue(':descricao', 'Juiz Substituto');
$statement->bindValue(':carga_horaria', 20);
$statement->bindValue(':dias_semana', 5);

if ($statement->executeStatement())
{
    echo "Cargo incluído com sucesso.\n";
}

////////// Adicionar Menus /////////////////////

$sqlInsertMenu = "INSERT INTO Menu (descricao, alias) VALUES (:descricao, :alias);";
$statement = $entityManager->getConnection()->prepare($sqlInsertMenu);

$statement->bindValue(':descricao', 'Cargos');
$statement->bindValue(':alias', 'cargo');
$statement->executeStatement();

$statement->bindValue(':descricao', 'Lotações');
$statement->bindValue(':alias', 'lotacao');
$statement->executeStatement();

$statement->bindValue(':descricao', 'Usuários');
$statement->bindValue(':alias', 'usuario');

if ($statement->executeStatement())
{
    echo "Menu incluído com sucesso.\n";
}

////////// Adicionar Usuarios /////////////////////

$sqlInsertUsuario = "INSERT INTO Funcionario (nome, matricula, login, senha, horario_inicio, horario_fim, lotacao_id, cargo_id) VALUES (:nome, :matricula, :login, :senha, :horario_inicio, :horario_fim, :lotacao_id, :cargo_id);";
$statement = $entityManager->getConnection()->prepare($sqlInsertUsuario);

$statement->bindValue(':nome', 'Marília Mendonça');
$statement->bindValue(':matricula', '123001');
$statement->bindValue(':login', 'mme10');
$statement->bindValue(':senha', password_hash('123123', PASSWORD_ARGON2I));
$statement->bindValue(':horario_inicio', DateTime::createFromFormat('H:i', '11:00')->format('H:i'));
$statement->bindValue(':horario_fim', DateTime::createFromFormat('H:i', '19:00')->format('H:i'));
$statement->bindValue(':lotacao_id', 1);
$statement->bindValue(':cargo_id', 6);
$statement->executeStatement();

$statement->bindValue(':nome', 'Gilberto Gil');
$statement->bindValue(':matricula', '123002');
$statement->bindValue(':login', 'ggi10');
$statement->bindValue(':senha', password_hash('123123', PASSWORD_ARGON2I));
$statement->bindValue(':horario_inicio', DateTime::createFromFormat('H:i', '11:00')->format('H:i'));
$statement->bindValue(':horario_fim', DateTime::createFromFormat('H:i', '19:00')->format('H:i'));
$statement->bindValue(':lotacao_id', 3);
$statement->bindValue(':cargo_id', 4);
$statement->executeStatement();

$statement->bindValue(':nome', 'Maju Flores');
$statement->bindValue(':matricula', '123003');
$statement->bindValue(':login', 'mfl10');
$statement->bindValue(':senha', password_hash('123123', PASSWORD_ARGON2I));
$statement->bindValue(':horario_inicio', DateTime::createFromFormat('H:i', '11:00')->format('H:i'));
$statement->bindValue(':horario_fim', DateTime::createFromFormat('H:i', '19:00')->format('H:i'));
$statement->bindValue(':lotacao_id', 8);
$statement->bindValue(':cargo_id', 2);
$statement->executeStatement();

$statement->bindValue(':nome', 'Bruno Gagliasso');
$statement->bindValue(':matricula', '123004');
$statement->bindValue(':login', 'bga10');
$statement->bindValue(':senha', password_hash('123123', PASSWORD_ARGON2I));
$statement->bindValue(':horario_inicio', DateTime::createFromFormat('H:i', '11:00')->format('H:i'));
$statement->bindValue(':horario_fim', DateTime::createFromFormat('H:i', '19:00')->format('H:i'));
$statement->bindValue(':lotacao_id', 8);
$statement->bindValue(':cargo_id', 1);

if ($statement->executeStatement())
{
    echo "Usuário incluído com sucesso.\n";
}

////////// Adicionar Autorização /////////////////////

$sqlInsertAutorizacao = "INSERT INTO Autorizacao (funcionario_id, menu_id) VALUES (:funcionario_id, :menu_id);";
$statement = $entityManager->getConnection()->prepare($sqlInsertAutorizacao);

$statement->bindValue(':funcionario_id', 1);
$statement->bindValue(':menu_id', 1);
$statement->executeStatement();
$statement->bindValue(':funcionario_id', 1);
$statement->bindValue(':menu_id', 2);
$statement->executeStatement();
$statement->bindValue(':funcionario_id', 1);
$statement->bindValue(':menu_id', 3);
$statement->executeStatement();

$statement->bindValue(':funcionario_id', 2);
$statement->bindValue(':menu_id', 1);
$statement->executeStatement();
$statement->bindValue(':funcionario_id', 2);
$statement->bindValue(':menu_id', 2);
$statement->executeStatement();

$statement->bindValue(':funcionario_id', 3);
$statement->bindValue(':menu_id', 1);

if ($statement->executeStatement())
{
    echo "Autorização incluída com sucesso.\n";
}