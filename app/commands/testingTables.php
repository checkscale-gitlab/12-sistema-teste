<?php

use App\Entity\Cargo;
use App\Entity\Cidade;
use App\Entity\Funcionario;
use App\Entity\Lotacao;
use App\Entity\Menu;
use App\Infra\EntityManagerCreator;

require_once __DIR__ . '/../vendor/autoload.php';

$entityManagerCreator = new EntityManagerCreator();
$entityManager = $entityManagerCreator->getEntityManager();

// $novoCargo = new Cargo();
// $novoCargo->setDescricao("Analista Administrativo");
// $novoCargo->setCargaHoraria(40);
// $novoCargo->setDiasSemana(5);
// $entityManager->persist($novoCargo);

// $novaCidade = new Cidade();
// $novaCidade->setNome("Curitiba");
// $novaCidade->setSigla("CTB");
// $entityManager->persist($novaCidade);

// $novaLotacao = new Lotacao();
// $novaLotacao->setNome("20ª Vara Federal de Curitiba");
// $novaLotacao->setSigla("PRCTB20");
// $novaLotacao->setCidade($novaCidade);
// $entityManager->persist($novaLotacao);

// $novoMenu = new Menu();
// $novoMenu->setDescricao("Cargos");
// $entityManager->persist($novoMenu);

// $novoFuncionario = new Funcionario();
// $novoFuncionario->setNome("Ana Maria Fernandez");
// $novoFuncionario->setMatricula("123633");
// $novoFuncionario->setCargo($novoCargo);
// $novoFuncionario->setLotacao($novaLotacao);
// $novoFuncionario->setLogin("amf10");
// $novoFuncionario->setSenha("123123");
// $novoFuncionario->addAutorizacao($novoMenu);
// $novoFuncionario->setHorarioInicio(DateTime::createFromFormat('H:i', '11:00'));
// $novoFuncionario->setHorarioFim(DateTime::createFromFormat('H:i', '19:00'));
// $entityManager->persist($novoFuncionario);

// $entityManager->flush();

// echo $novoFuncionario->getNome() . "\n\n";
echo "olá\n";