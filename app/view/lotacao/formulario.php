<?php include __DIR__ . '/../inicio-html.php'; ?>

<div class="container-fluid p-0 d-flex justify-content-between">
    <span>
        <h1><?= $titulo; ?></h1>
    </span>
    <span>
        <?php if (!$formDisabled) { ?>
            <button type="submit" form="formulario-lotacao" class="btn btn-primary">Salvar</button>
        <?php } ?>
        <button class="btn btn-secondary" onclick="document.location.href='/listar-lotacoes'">Cancelar</button>
    </span>

    
</div>

<form id="formulario-lotacao" action="/salvar-lotacao<?= isset($lotacao) ? '?id=' . $lotacao->getId() : ''; ?>" method="post">
    <fieldset class="form-group" <?= $formDisabled ? "disabled" : ""; ?>>
        <label for="nome">Nome</label>
        <input type="text" id="nome" name="nome" required class="form-control mb-3" value="<?= (isset($lotacao) && $lotacao->getNome() != null) ? $lotacao->getNome() : ''; ?>">

        <label for="sigla">Sigla</label>
        <input type="text" id="sigla" name="sigla" required class="form-control mb-3" value="<?= (isset($lotacao) && $lotacao->getSigla() != null) ? $lotacao->getSigla() : ''; ?>">

        <label for="cidade">Cidade:</label>
        <select id="cidade" name="cidade" required class="form-control mb-3" >
            <option value="">Escolha uma opção</option>
            <?php 
                if ($cidades) {
                    foreach($cidades as $cidade) { 
                        $selectedCidade = false;
                        if (isset($lotacao) && $lotacao->getcidade() != null && $lotacao->getcidade()->getId() == $cidade->getId()) {
                            $selectedCidade = true;
                        }?>
                        <option value="<?= $cidade->getId(); ?>" <?= $selectedCidade ? "selected" : ""; ?>><?= $cidade->getNome(); ?></option><?php
                    }
                }
            ?>
        </select>
    </fieldset>
</form>

<?php include __DIR__ . '/../fim-html.php'; ?>