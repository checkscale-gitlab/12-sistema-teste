<?php include __DIR__ . '/../inicio-html.php'; ?>

<div class="container-fluid p-0 d-flex justify-content-between">
    <span>
        <h1><?= $titulo; ?></h1>
    </span>
    <span>
        <button type="button" class="btn btn-primary" onclick="document.location.href='/novo-cargo'">Novo</button>
        <button type="button" class="btn btn-secondary" onclick="document.location.href='/home'">Fechar</button>
    </span>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Descrição</th>
            <th scope="col" class="min-vw-25 d-flex justify-content-end">Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($cargos as $cargo) { ?>
            <tr>
                <td><?= $cargo->getDescricao(); ?></td>
                <td class="min-vw-25 d-flex justify-content-end">
                    <a href="/consultar-cargo?id=<?= $cargo->getId(); ?>" class="table-link">
                        <span class="visually-hidden">Consultar cargo <?= $cargo->getDescricao(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-search"></i>
                        </span>
                    </a>
                    <a href="/alterar-cargo?id=<?= $cargo->getId(); ?>" class="table-link">
                        <span class="visually-hidden">Alterar cargo <?= $cargo->getDescricao(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-pencil-square"></i>
                        </span>
                    </a>
                    <a href="/excluir-cargo?id=<?= $cargo->getId(); ?>" class="table-link danger">
                        <span class="visually-hidden">Excluir cargo <?= $cargo->getDescricao(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-trash"></i>
                        </span>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php include __DIR__ . '/../fim-html.php'; ?>