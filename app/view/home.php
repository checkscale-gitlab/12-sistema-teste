<?php include __DIR__ . '/inicio-html.php'; ?>


<?php if (isset($_SESSION['mensagem'])): ?>
    <div class="alert alert-<?= $_SESSION['tipo_mensagem']; ?>">
        <?= $_SESSION['mensagem']; ?>
    </div>
    <?php
    unset($_SESSION['mensagem']);
    unset($_SESSION['tipo_mensagem']);
endif;
?>

<div class="w-100 h-90 d-flex justify-content-center">
    <h1><?= $titulo; ?></h1>
</div>


<?php include __DIR__ . '/fim-html.php'; ?>