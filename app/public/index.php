<?php

// echo "Hello, World!";
// var_dump($_SERVER);

require __DIR__ . '/../vendor/autoload.php';

use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;

$caminho = $_SERVER['PATH_INFO'];
$rotas = require __DIR__ . '/../config/routes.php';

if (!array_key_exists($caminho, $rotas)) {
    // http_response_code(404);
    // exit();
    header('Location: /login');
    exit();
}

session_start();

$ehRotaDeLogin = stripos($caminho, 'login');
$ehRotaDeUsuario = stripos($caminho, 'usuario');
$ehRotaDeCargo = stripos($caminho, 'cargo');
$ehRotaDeLotacao = stripos($caminho, 'lotac');
if (!isset($_SESSION['logado']) && $ehRotaDeLogin === false) {
    header('Location: /login');
    exit();
}
if (isset($_SESSION['logado']) && !isset($_SESSION['autorizacao-usuario']) && $ehRotaDeUsuario) {
    header('Location: /home');
    exit();
}
if (isset($_SESSION['logado']) && !isset($_SESSION['autorizacao-cargo']) && $ehRotaDeCargo) {
    header('Location: /home');
    exit();
}
if (isset($_SESSION['logado']) && !isset($_SESSION['autorizacao-lotacao']) && $ehRotaDeLotacao) {
    header('Location: /home');
    exit();
}

// fábrica que implementa as interfaces da PSR17
$psr17Factory = new Psr17Factory();

// criador de requests
$creator = new ServerRequestCreator(
    $psr17Factory, // ServerRequestFactory
    $psr17Factory, // UriFactory
    $psr17Factory, // UploadedFileFactory
    $psr17Factory  // StreamFactory
);

$request = $creator->fromGlobals(); // a partir dos super globais $_GET $_POST $_SERVER, monta a request


$classeControladora = $rotas[$caminho];
$container = require __DIR__ . '../../config/dependencies.php';
$controlador = $container->get($classeControladora);
$response = $controlador->handle($request);

// enviar o header para o navegador (client)
foreach ($response->getHeaders() as $name => $values) {
    foreach ($values as $value) {
        header(sprintf('%s: %s', $name, $value), false);
    }
}
echo $response->getBody();