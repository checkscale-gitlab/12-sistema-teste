<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126190922 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Autorizacao (funcionario_id INT NOT NULL, menu_id INT NOT NULL, INDEX IDX_472BBD1F642FEB76 (funcionario_id), INDEX IDX_472BBD1FCCD7E912 (menu_id), PRIMARY KEY(funcionario_id, menu_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Autorizacao ADD CONSTRAINT FK_472BBD1F642FEB76 FOREIGN KEY (funcionario_id) REFERENCES Funcionario (id)');
        $this->addSql('ALTER TABLE Autorizacao ADD CONSTRAINT FK_472BBD1FCCD7E912 FOREIGN KEY (menu_id) REFERENCES Menu (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE Autorizacao');
    }
}
