<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Cidade 
{
    /**
     * @Id
     * @GeneratedValue
     * @Column (type="integer")
     */
    private $id;
    /**
     * @Column (type="string")
     */
    private $sigla;
    /**
     * @Column (type="string")
     */
    private $nome;
    /**
     * @OneToMany (targetEntity="Lotacao", mappedBy="cidade", cascade={"persist"})
     */
    private $lotacoes;

    public function __construct()
    {
        $this->lotacoes = new ArrayCollection();
    }

    public function addLotacao(Lotacao $lotacao): self
    {
        if ($this->lotacoes->contains($lotacao)) {
            return $this;
        }

        $this->lotacoes->add($lotacao);
        $lotacao->setCidade($this);
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }

    public function getSigla(): string
    {
        return $this->sigla;
    }

    public function setSigla(string $sigla): self
    {
        $this->sigla = $sigla;
        return $this;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;
        return $this;
    }
}