<?php

namespace App\Controller\Lotacao;

use App\Entity\Cidade;
use App\Entity\Lotacao;
use App\Helper\FlashMessageTrait;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FormularioConsultarLotacao implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeLotacoes;
    private $repositorioDeCidades;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeLotacoes = $entityManager->getRepository(Lotacao::class);
        $this->repositorioDeCidades = $entityManager->getRepository(Cidade::class);
    }
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Lotação inválida.');
            return new Response(302, ['Location' => '/listar-lotacoes']);
        }

        $lotacao = $this->repositorioDeLotacoes->find($idEntidade);
        $cidades = [];
        array_push($cidades, $lotacao->getCidade());

        $html = $this->renderizaHtml('/lotacao/formulario.php', [
            'titulo' => 'Consultar lotação',
            'lotacao' => $lotacao,
            'cidades' => $cidades,
            'formDisabled' => true
        ]);

        return new Response(200, [], $html);
    }
}