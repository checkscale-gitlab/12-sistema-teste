<?php

namespace App\Controller\Cargo;

use App\Entity\Cargo;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PersistenciaCargo implements RequestHandlerInterface
{
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeCargos;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeCargos = $entityManager->getRepository(Cargo::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $bodyString = $request->getParsedBody();
        $descricao = filter_var($bodyString['descricao'], FILTER_SANITIZE_STRING);
        $cargaHoraria = filter_var($bodyString['carga_horaria'], FILTER_SANITIZE_NUMBER_INT);
        $diasSemana = filter_var($bodyString['dias_semana'], FILTER_SANITIZE_NUMBER_INT);

        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (!is_null($idEntidade) && $idEntidade !== false) {
            # PUT request
            $entity = $this->repositorioDeCargos->find($idEntidade);
            $entity->setDescricao($descricao);
            $entity->setCargaHoraria($cargaHoraria);
            $entity->setDiasSemana($diasSemana);

            $this->defineMensagem('success', 'Cargo alterado com sucesso.');

        } else {
            # POST request
            $cargo = new Cargo();
            $cargo->setDescricao($descricao);
            $cargo->setCargaHoraria($cargaHoraria);
            $cargo->setDiasSemana($diasSemana);

            $this->entityManager->persist($cargo);
            $this->defineMensagem('success', 'Cargo criado com sucesso.');
        }

        $this->entityManager->flush();
        return new Response(200, ['Location' => '/listar-cargos']);
    }
}