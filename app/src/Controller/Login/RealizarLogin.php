<?php

namespace App\Controller\Login;

use App\Entity\Funcionario;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RealizarLogin implements RequestHandlerInterface
{
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeUsuarios;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeUsuarios = $entityManager->getRepository(Funcionario::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $bodyString = $request->getParsedBody();

        $login = filter_var($bodyString['login'], FILTER_SANITIZE_STRING);
        if (is_null($login) || $login === false) {
            $this->defineMensagem('danger', 'Usuário inválido.');
            return new Response(400, ['Location' => '/login']);
        }

        $senha = filter_var($bodyString['senha'], FILTER_SANITIZE_STRING);
        $usuario = $this->repositorioDeUsuarios->findOneBy(['login' => $login]);

        if (is_null($usuario) || !$usuario->verificaSenha($senha)) {
            $this->defineMensagem('danger', 'Usuário e/ou senha inválidos.');
            return new Response(401, ['Location' => '/login']);
        }

        $autorizacoes = $usuario->getAutorizacoes();
        if (count($autorizacoes) === 0 || is_null($autorizacoes)) {
            $this->defineMensagem('danger', 'Usuário não tem autorização de acesso aos menus.');
        } else {
            foreach ($autorizacoes as $menu) {
                $_SESSION["autorizacao-{$menu->getAlias()}"] = true;
            }
        }

        $_SESSION['logado'] = true;
        return new Response(200, ['Location' => '/home']);
    }
}